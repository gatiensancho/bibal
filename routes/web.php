<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('emprunts', 'EmpruntController');
Route::resource('exemplaires', 'ExemplaireController');
Route::resource('oeuvres', 'OeuvreController');
Route::resource('reservations', 'ReservationController');
Route::resource('usagers', 'UsagerController');

Route::get('/', function() {
    return redirect()->route('usagers.index');
})->name('home');
