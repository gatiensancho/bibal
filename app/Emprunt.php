<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emprunt extends Model
{
    function getExemplaire() {
        return $this->hasOne('App\Exemplaire', 'id', 'Exemplaire');
    }

    function getUsager() {
        return $this->hasOne('App\Usager', 'id', 'Usager');
    }
}
