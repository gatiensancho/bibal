<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    function getUsager() {
        return $this->hasOne('App\Usager', 'id', 'Usager');
    }

    function getOeuvre() {
        return $this->hasOne('App\Oeuvre', 'id', 'Oeuvre');
    }
}
