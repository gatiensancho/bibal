<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exemplaire extends Model
{
    function getOeuvre() {
        return $this->hasOne('App\Oeuvre', 'id' ,'Oeuvre');
    }

    function getEmprunts() {
        return $this->hasMany('App\Emprunt', 'Exemplaire');
    }

    function Editer($reference) {
        $this->reference = $reference;
        $this->save();
    }
}
