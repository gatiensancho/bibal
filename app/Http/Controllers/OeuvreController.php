<?php

namespace App\Http\Controllers;

use App\Oeuvre;
use Illuminate\Http\Request;

class OeuvreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oeuvres = Oeuvre::orderBy('titre')->get();
        return view('bibal/oeuvres/index', compact(
            'oeuvres'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $oeuvre = new Oeuvre();
        return view('bibal/oeuvres/create', compact('oeuvre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $oeuvre = new Oeuvre();
        $oeuvre->titre = $request->get('titre');
        $oeuvre->auteur = $request->get('auteur');
        $oeuvre->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Oeuvre  $oeuvre
     * @return \Illuminate\Http\Response
     */
    public function show(Oeuvre $oeuvre)
    {
        return view('bibal/oeuvres/show', compact(
            'oeuvre'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Oeuvre  $oeuvre
     * @return \Illuminate\Http\Response
     */
    public function edit(Oeuvre $oeuvre)
    {
        return view('bibal/oeuvres/edit', compact(
            'oeuvre'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Oeuvre  $oeuvre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Oeuvre $oeuvre)
    {
        $oeuvre->Editer($request->get('auteur'), $request->get('titre'));
    }
}
