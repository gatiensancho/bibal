<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Usager;
use App\Oeuvre;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReservationController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $oeuvres = Oeuvre::pluck('titre', 'id');
        $usagers = Usager::pluck('nom', 'id');
        $reservation = new Reservation();
        return view('bibal/reservations/create', compact(
            'reservation',
            'oeuvres',
            'usagers'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reservation = new Reservation;
        $reservation->date = Carbon::now();
        $reservation->Oeuvre = $request->get('id_oeuvre');
        $reservation->Usager = $request->get('id_usager');
        $reservation->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        $reservation->delete();
    }
}
