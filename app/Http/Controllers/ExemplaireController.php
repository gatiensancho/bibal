<?php

namespace App\Http\Controllers;

use App\Exemplaire;
use App\Oeuvre;
use Illuminate\Http\Request;

class ExemplaireController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $exemplaire = new Exemplaire;
        $oeuvres = Oeuvre::pluck('titre', 'id');
        return view('bibal/exemplaires/create', compact(
            'exemplaire',
            'oeuvres'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $exemplaire = new Exemplaire();
        $exemplaire->reference = $request->get('reference');
        $exemplaire->Oeuvre = $request->get('id_oeuvre');
        $exemplaire->disponible = true;

        $exemplaire->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Exemplaire  $exemplaire
     * @return \Illuminate\Http\Response
     */
    public function edit(Exemplaire $exemplaire)
    {
        return view('bibal/exemplaires/edit', compact(
            'exemplaire'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Exemplaire  $exemplaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exemplaire $exemplaire)
    {
        $exemplaire->Editer($request->get('reference'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Exemplaire  $exemplaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exemplaire $exemplaire)
    {
        // On supprime les emprunts associés à cet exemplaire
        foreach ($exemplaire->getEmprunts as $emprunt) $emprunt->delete();
        $exemplaire->delete();
    }
}
