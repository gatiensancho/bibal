<?php

namespace App\Http\Controllers;

use App\Exemplaire;
use App\Usager;
use Illuminate\Http\Request;

class UsagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usagers = Usager::orderBy('nom')->get();
        return view('bibal/usagers/index', compact(
            'usagers'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usager = new Usager;
        return view('bibal/usagers/create', compact('usager'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usager = new Usager();
        $usager->nom = $request->get('nom');
        $usager->prenom = $request->get('prenom');
        $usager->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usager  $usager
     * @return \Illuminate\Http\Response
     */
    public function show(Usager $usager)
    {
        return view('bibal/usagers/show', compact(
            'usager'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usager  $usager
     * @return \Illuminate\Http\Response
     */
    public function edit(Usager $usager)
    {
        return view('bibal/usagers/edit', compact(
            'usager'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usager  $usager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Usager $usager)
    {
        $usager->Editer($request->get('nom'), $request->get('prenom'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usager  $usager
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usager $usager)
    {
        // Suppression des réservations
        foreach ($usager->getReservations as $reservation) $reservation->delete();

        foreach ($usager->getEmprunts as $emprunt) {
            // Tous les exemplaires actuellement empruntés : rendre disponibles
            $exemplaire = Exemplaire::find($emprunt->getExemplaire->id);
            $exemplaire->disponible = true;
            $exemplaire->save();

            // Suppression des emprunts
            $emprunt->delete();
        }
        $usager->delete();
    }
}
