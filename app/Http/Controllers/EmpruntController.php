<?php

namespace App\Http\Controllers;

use App\Emprunt;
use App\Exemplaire;
use App\Reservation;
use App\Usager;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmpruntController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $exemplaires = Exemplaire::where('disponible', '<>', 0)->get();
        $usagers = Usager::pluck('nom', 'id');
        $emprunt = new Emprunt();
        return view('bibal/emprunts/create', compact(
            'exemplaires',
            'usagers',
            'emprunt'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emprunt = new Emprunt;
        $emprunt->date = Carbon::now();
        $emprunt->Exemplaire = $request->get('id_exemplaire');
        $emprunt->Usager = $request->get('id_usager');

        $emprunt->save();

        // Exemplaire non disponible !
        $exemplaire = Exemplaire::findOrFail($request->get('id_exemplaire'));
        $exemplaire->disponible = false;
        $exemplaire->save();

        // Si réservation(s) avec même user et même oeuvre, on la/les supprime
        $reservations = Reservation::where('Usager', $request->get('id_usager'))->where('Oeuvre', $exemplaire->getOeuvre->id)->get();
        foreach ($reservations as $reservation) {
            $reservation->delete();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Emprunt  $emprunt
     * @return \Illuminate\Http\Response
     */
    public function destroy(Emprunt $emprunt)
    {
        $exemplaire = Exemplaire::findOrFail($emprunt->getExemplaire->id);
        $exemplaire->disponible = true;
        $exemplaire->save();
        $emprunt->delete();
    }
}
