<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oeuvre extends Model
{
    function getExemplaires() {
        return $this->hasMany('App\Exemplaire', 'Oeuvre');
    }

    function getReservations() {
        return $this->hasMany('App\Reservation', 'Oeuvre');
    }
    function getEmprunts() {
        return $this->hasManyThrough(
            'App\Emprunt',
            'App\Exemplaire',
            'Oeuvre',
            'Exemplaire',
            'id',
            'id'
        );
    }

    function Editer($auteur, $titre) {
        $this->auteur = $auteur;
        $this->titre = $titre;
        $this->save();
    }

    function TrouverExemplaireDispo() {
        $exemplaireReturn = false;
        foreach ($this->getExemplaires() as $exemplaire) {
            if($exemplaire->disponible) {
                $exemplaireReturn = $exemplaire;
            }
        }
        return $exemplaireReturn;
    }
}
