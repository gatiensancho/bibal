<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usager extends Model
{
    function getReservations() {
        return $this->hasMany('App\Reservation', 'Usager', 'id');
    }

    function getEmprunts() {
        return $this->hasMany('App\Emprunt', 'Usager', 'id');
    }

    function Editer($nom, $prenom) {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->save();
    }
}
