@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item active">Exemplaire - Modifier</li>
    </ol>
    <div class="row">
        {{ Form::model($exemplaire, array('onsubmit' => 'event.preventDefault(); modifierIHM();')) }}
            {{ Form::label('reference', 'Référence') }}
            {{ Form::text('reference') }}
            {{ Form::submit('Modifier l\'exemplaire') }}
        {{ Form::close() }}
    </div>
    <div class="row">
        <br><br>
        <a class="btn btn-warning" onclick="supprimerIHM()" id="toggleNavPosition">Supprimer l'exemplaire</a>
    </div>

    <script>
        function modifierIHM() {
            $.ajax({
                url: '{{ route('exemplaires.update', ['exemplaire' => $exemplaire->id]) }}',
                data: $("form").serialize(),
                type: 'PUT',
                success: function(result) {
                    window.location.replace('{{ route('oeuvres.show', ['oeuvre' => $exemplaire->getOeuvre->id]) }}');
                }
            });
        }
        function supprimerIHM() {
            $.ajax({
                url: '{{ route('exemplaires.destroy', ['exemplaire' => $exemplaire->id]) }}',
                data: { "_token": "{{ csrf_token() }}"},
                type: 'DELETE',
                success: function(result) {
                    window.location.replace('{{ route('oeuvres.show', ['oeuvre' => $exemplaire->getOeuvre->id]) }}');
                }
            });
        }
    </script>
@endsection