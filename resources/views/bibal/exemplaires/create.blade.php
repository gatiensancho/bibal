@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item active">Exemplaires - Ajouter</li>
    </ol>


    <div class="row">
        {{ Form::model($exemplaire, array('onsubmit' => 'event.preventDefault(); ajouterIHM();')) }}
            <p>
                {{ Form::label('reference', 'Référence') }}
                {{ Form::text('reference') }}
            </p>
            <p>
                {{ Form::label('id_oeuvre', 'Oeuvre') }}
                {{ Form::select('id_oeuvre', $oeuvres)  }}
            </p>
            {{ Form::submit('Ajouter l\'exemplaire') }}
        {{ Form::close() }}
    </div>
    <script>
        function ajouterIHM() {
            $.ajax({
                url: '{{ route('exemplaires.store') }}',
                data: $("form").serialize(),
                type: 'POST',
                success: function(result) {
                    window.location.replace('/oeuvres/'+$( "[name='id_oeuvre']" ).find(":selected").val());
                }
            });
        }
    </script>
@endsection