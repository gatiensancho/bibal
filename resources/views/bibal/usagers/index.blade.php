@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item active">Usagers</li>
    </ol>


    <div class="row">
        <div class="col-3">
            <a class="btn btn-primary btn-block" onclick="ajouterUsagerIHM()"><i class="fa fa-plus fa-fw" aria-hidden="true"></i>Ajouter un usager</a><br>
            <script>
                function ajouterUsagerIHM() {
                    window.location.replace("{{ route('usagers.create') }}");
                }
            </script>
        </div>
        <div class="col-12">
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table fa-fw" aria-hidden="true"></i>Usagers
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nom / prénom</th>
                            <th>Réservations</th>
                            <th>Emprunts</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($usagers as $usager)
                                <tr>
                                    <td><a style="color:#0056b3; cursor: pointer" onclick="afficherUsagerIHM('{{ route('usagers.show', ['usager' => $usager->id]) }}')">{{ $usager->nom }} {{ $usager->prenom }}</a></td>
                                    <td>{{ count($usager->getReservations) }}</td>
                                    <td>{{ count($usager->getEmprunts) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <script>
                            function afficherUsagerIHM(url) {
                                window.location.replace(url);
                            }
                        </script>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection