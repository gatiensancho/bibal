@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item active">Usagers - Ajouter</li>
    </ol>


    <div class="row">
        {{ Form::model($usager, array('onsubmit' => 'event.preventDefault(); ajouterIHM();')) }}
            {{ Form::label('nom', 'Nom') }}
            {{ Form::text('nom') }}
            {{ Form::label('prenom', 'Prénom') }}
            {{ Form::text('prenom') }}
            {{ Form::submit('Ajouter l\'usager') }}
        {{ Form::close() }}
    </div>
    <script>
        function ajouterIHM() {
            $.ajax({
                url: '{{ route('usagers.store') }}',
                data: $("form").serialize(),
                type: 'POST',
                success: function(result) {
                    window.location.replace('{{ route('usagers.index') }}');
                }
            });
        }
    </script>
@endsection