@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('usagers.index') }}">Usagers</a>
        </li>
        <li class="breadcrumb-item active">{{ $usager->nom }} {{ $usager->prenom }}</li>
    </ol>

    <h1><i class="fa fa-user fa-fw" aria-hidden="true"></i>{{ $usager->nom }} {{ $usager->prenom }}</h1>
    <p><small><a onclick="editerUsagerIHM()"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i>Modifier / supprimer</a></small></p>
    <script>
        function editerUsagerIHM() {
            window.location.replace("{{ route('usagers.edit', ['usager' => $usager->id]) }}");
        }
    </script>



    <div class="row">
        <div class="col-6">
            <div class="card-header">
                <i class="fa fa-table"></i> Réservations <a onclick="reserverOeuvreIHM()">+ Ajouter</a>
                <script>
                    function reserverOeuvreIHM() {
                        window.location.replace("{{ route('reservations.create') }}");
                    }
                </script>
            </div>
            <div class="card-body">
                @if(count($usager->getReservations))
                <table class="table">
                    <thead>
                        <th>Date</th>
                        <th>Oeuvre</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach($usager->getReservations as $reservation)
                        <tr>
                            <td>
                                <a href="{{ route('reservations.show', ['reservation' => $reservation->id]) }}">{{ \Carbon\Carbon::parse($reservation->date)->format('d/m/Y') }}</a>
                            </td>
                            <td>
                                <a href="{{ route('oeuvres.show', ['oeuvre' => $reservation->getOeuvre->id]) }}">{{ $reservation->getOeuvre->titre }} ({{ $reservation->getOeuvre->auteur }})</a>
                            </td>
                            <td>
                                <a class="btn btn-danger" onclick="annulerReservationIHM({{ $reservation->id }})" id="toggleNavPosition">Annuler</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                <h3><i>Aucune réservation</i></h3>
                @endif
            </div>
        </div>
        <div class="col-6">
            <div class="card-header">
                <i class="fa fa-table"></i> Emprunts <a onclick="emprunterExemplaireIHM()">+ Ajouter</a>
                <script>
                    function emprunterExemplaireIHM() {
                        window.location.replace("{{ route('emprunts.create') }}");
                    }
                </script>
            </div>
            <div class="card-body">
                @if(count($usager->getEmprunts))
                <table class="table">
                    <thead>
                    <th>Date</th>
                    <th>Oeuvre / Exemplaire</th>
                    <th>Action</th>
                    </thead>
                    <tbody>
                    @foreach($usager->getEmprunts as $emprunt)
                        <tr>
                            <td>
                                <a href="{{ route('emprunts.show', ['emprunt' => $emprunt->id]) }}">{{ \Carbon\Carbon::parse($emprunt->date)->format('d/m/Y') }}</a>
                            </td>
                            <td>
                                <a href="{{ route('oeuvres.show', ['oeuvre' => $emprunt->getExemplaire->getOeuvre->id]) }}">{{ $emprunt->getExemplaire->getOeuvre->titre }} ({{ $emprunt->getExemplaire->getOeuvre->auteur }})</a> / #{{ $emprunt->getExemplaire->reference }}
                            </td>
                            <td>
                                <a class="btn btn-warning" onclick="rendreExemplaireIHM({{ $emprunt->id }})" id="toggleNavPosition">Rendre</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                    <h3><i>Aucun emprunt</i></h3>
                @endif
            </div>
        </div>
    </div>
    <script>
        function annulerReservationIHM(idReservation) {
            $.ajax({
                url: '/reservations/'+idReservation,
                data: { "_token": "{{ csrf_token() }}"},
                type: 'DELETE',
                success: function(result) {
                    window.location.reload();
                }
            });
        }
        function rendreExemplaireIHM(idEmprunt) {
            $.ajax({
                url: '/emprunts/'+idEmprunt,
                data: { "_token": "{{ csrf_token() }}"},
                type: 'DELETE',
                success: function(result) {
                    window.location.reload();
                }
            });
        }
    </script>
@endsection