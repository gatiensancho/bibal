@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item active">Usagers - Modifier</li>
    </ol>


    <div class="row">
        {{ Form::model($usager, array('onsubmit' => 'event.preventDefault(); modifierIHM();')) }}
            {{ Form::label('nom', 'Nom') }}
            {{ Form::text('nom') }}
            {{ Form::label('prenom', 'Prénom') }}
            {{ Form::text('prenom') }}
            {{ Form::submit('Modifier l\'usager') }}
        {{ Form::close() }}
    </div>
    <div class="row">
        <br><br>
        <a class="btn btn-warning" onclick="supprimerIHM()" id="toggleNavPosition">Supprimer l'usager</a>
    </div>

    <script>
        function modifierIHM() {
            $.ajax({
                url: '{{ route('usagers.update', ['usager' => $usager->id]) }}',
                data: $("form").serialize(),
                type: 'PUT',
                success: function(result) {
                    window.location.replace('{{ route('usagers.show', ['usager' => $usager->id]) }}');
                }
            });
        }
        function supprimerIHM() {
            $.ajax({
                url: '{{ route('usagers.destroy', ['usager' => $usager->id]) }}',
                data: { "_token": "{{ csrf_token() }}"},
                type: 'DELETE',
                success: function(result) {
                    window.location.replace('{{ route('usagers.index') }}');
                }
            });
        }
    </script>
@endsection