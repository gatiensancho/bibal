@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item active">Emprunt - Ajouter</li>
    </ol>


    <div class="row">
        {{ Form::model($emprunt, array('onsubmit' => 'event.preventDefault(); ajouterIHM();')) }}
            <p>
                {{ Form::label('id_exemplaire', 'Exemplaire') }}
                <select name="id_exemplaire">
                    @foreach($exemplaires as $exemplaire)
                        <option value="{{ $exemplaire->id }}">{{ $exemplaire->getOeuvre->titre }} ({{ $exemplaire->getOeuvre->auteur }}) ex. {{ $exemplaire->reference }}</option>
                    @endforeach
                </select>
            </p>
            <p>
                {{ Form::label('id_usager', 'Usager') }}
                {{ Form::select('id_usager', $usagers)  }}
            </p>
            {{ Form::submit('Ajouter l\'emprunt') }}
        {{ Form::close() }}
    </div>
    <script>
        function ajouterIHM() {
            $.ajax({
                url: '{{ route('emprunts.store') }}',
                data: $("form").serialize(),
                type: 'POST',
                success: function(result) {
                    window.location.replace('/usagers/'+$( "[name='id_usager']" ).find(":selected").val());
                }
            });
        }
    </script>
@endsection