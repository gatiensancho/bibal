@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item active">Reservations - Ajouter</li>
    </ol>


    <div class="row">
        {{ Form::model($reservation, array('onsubmit' => 'event.preventDefault(); ajouterIHM();')) }}
            <p>
                {{ Form::label('id_oeuvre', 'Oeuvre') }}
                {{ Form::select('id_oeuvre', $oeuvres)  }}
            </p>
            <p>
                {{ Form::label('id_usager', 'Usager') }}
                {{ Form::select('id_usager', $usagers)  }}
            </p>
            {{ Form::submit('Ajouter la réservations') }}
        {{ Form::close() }}
    </div>
    <script>
        function ajouterIHM() {
            $.ajax({
                url: '{{ route('reservations.store') }}',
                data: $("form").serialize(),
                type: 'POST',
                success: function(result) {
                    window.location.replace('/usagers/'+$( "[name='id_usager']" ).find(":selected").val());
                }
            });
        }
    </script>
@endsection