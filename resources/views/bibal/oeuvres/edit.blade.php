@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item active">Oeuvre - Modifier</li>
    </ol>
    <div class="row">
        {{ Form::model($oeuvre, array('onsubmit' => 'event.preventDefault(); modifierIHM();')) }}
            {{ Form::label('auteur', 'Auteur') }}
            {{ Form::text('auteur') }}
            {{ Form::label('titre', 'Titre') }}
            {{ Form::text('titre') }}
            {{ Form::submit('Modifier l\'oeuvre') }}
        {{ Form::close() }}
    </div>
    <script>
        function modifierIHM() {
            $.ajax({
                url: '{{ route('oeuvres.update', ['oeuvre' => $oeuvre->id]) }}',
                data: $("form").serialize(),
                type: 'PUT',
                success: function(result) {
                    window.location.replace('{{ route('oeuvres.show', ['oeuvre' => $oeuvre->id]) }}');
                }
            });
        }
    </script>
@endsection