@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('oeuvres.index') }}">Oeuvres</a>
        </li>
        <li class="breadcrumb-item active">{{ $oeuvre->titre }} ({{ $oeuvre->auteur }})</li>
    </ol>

    <h1><i class="fa fa-book fa-fw" aria-hidden="true"></i>{{ $oeuvre->titre }} ({{ $oeuvre->auteur }})</h1>
    <p><small><a onclick="editerOeuvreIHM()"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i>Modifier</a></small></p>
    <script>
        function editerOeuvreIHM() {
            window.location.replace('{{ route('oeuvres.edit', ['oeuvre' => $oeuvre->id]) }}');
        }
    </script>


    <div class="row">
        <div class="col-6">
            <div class="card-header">
                <i class="fa fa-table"></i> Réservations
            </div>
            <div class="card-body">
                @if(count($oeuvre->getReservations))
                <table class="table">
                    <thead>
                        <th>Date</th>
                        <th>Usager</th>
                    </thead>
                    <tbody>
                    @foreach($oeuvre->getReservations as $reservation)
                        <tr>
                            <td>
                                <a href="{{ route('reservations.show', ['reservation' => $reservation->id]) }}">{{ \Carbon\Carbon::parse($reservation->date)->format('d/m/Y') }}</a>
                            </td>
                            <td>
                                <a href="{{ route('usagers.show', ['usager' => $reservation->getUsager->id]) }}">{{ $reservation->getUsager->nom }} {{ $reservation->getUsager->prenom }}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                <h3><i>Aucune réservation</i></h3>
                @endif
            </div>
        </div>
        <div class="col-6">
            <div class="card-header">
                <i class="fa fa-table"></i> Exemplaires <a onclick="ajouterExemplaireIHM()">+ Ajouter</a>
                <script>
                    function ajouterExemplaireIHM() {
                        window.location.replace('{{ route('exemplaires.create') }}');
                    }
                </script>
            </div>
            <div class="card-body">
                @if(count($oeuvre->getExemplaires))
                <table class="table">
                    <thead>
                    <th>Référence</th>
                    <th>Disp. ?</th>
                    <th>Usager</th>
                    <th>Actions</th>
                    </thead>
                    <tbody>
                    @foreach($oeuvre->getExemplaires as $exemplaire)
                        <tr>
                            <td>
                                {{ $exemplaire->reference }}
                            </td>
                            <td>
                                @if(!$exemplaire->disponible)
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                @else
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                @endif
                            </td>
                            <td>
                                @if($exemplaire->disponible)
                                    -
                                @else
                                    <a href="{{ route('usagers.show', ['usager' => $exemplaire->getEmprunts[0]->getUsager->id]) }}">{{ $exemplaire->getEmprunts[0]->getUsager->nom }} {{$exemplaire->getEmprunts[0]->getUsager->prenom }}</a>
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-success" onclick="editerExemplaireIHM('{{ route('exemplaires.edit', ['exemplaire' => $exemplaire->id]) }}')" id="toggleNavPosition"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <script>
                        function editerExemplaireIHM(url) {
                            window.location.replace(url);
                        }
                    </script>
                </table>
                @else
                <h3><i>Aucun exemplaire</i></h3>
                @endif
            </div>
        </div>
    </div>
@endsection