@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item active">Oeuvres - Ajouter</li>
    </ol>
    <div class="row">
        {{ Form::model($oeuvre, array('onsubmit' => 'event.preventDefault(); ajouterIHM();')) }}
            {{ Form::label('auteur', 'Auteur') }}
            {{ Form::text('auteur') }}
            {{ Form::label('titre', 'Titre') }}
            {{ Form::text('titre') }}
            {{ Form::submit('Ajouter l\'oeuvre') }}
        {{ Form::close() }}
    </div>
    <script>
        function ajouterIHM() {
            $.ajax({
                url: '{{ route('oeuvres.store') }}',
                data: $("form").serialize(),
                type: 'POST',
                success: function(result) {
                    window.location.replace('{{ route('oeuvres.index') }}');
                }
            });
        }
    </script>
@endsection