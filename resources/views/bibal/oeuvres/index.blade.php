@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Bibal</a>
        </li>
        <li class="breadcrumb-item active">Oeuvres</li>
    </ol>


    <div class="row">
        <div class="col-3">
            <a class="btn btn-primary btn-block" onclick="ajouterIHM()"><i class="fa fa-plus fa-fw" aria-hidden="true"></i>Ajouter une oeuvre</a><br>
            <script>
                function ajouterIHM() {
                    window.location.replace('{{ route('oeuvres.create') }}');
                }
            </script>
        </div>
        <div class="col-12">
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table fa-fw" aria-hidden="true"></i>Oeuvres
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Auteur - Titre</th>
                            <th>Exemplaires</th>
                            <th>Emprunts</th>
                            <th>Réservations</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($oeuvres as $oeuvre)
                            <tr>
                                <td><a style="color:#0056b3; cursor: pointer" onclick="afficherOeuvreIHM('{{ route('oeuvres.show', ['oeuvre' => $oeuvre->id]) }}')">{{ $oeuvre->auteur }} - {{ $oeuvre->titre }}</a></td>
                                <td>{{ count($oeuvre->getExemplaires) }}</td>
                                <td>{{ count($oeuvre->getEmprunts) }}</td>
                                <td>{{ count($oeuvre->getReservations) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <script>
                            function afficherOeuvreIHM(url) {
                                window.location.replace(url);
                            }
                        </script>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection