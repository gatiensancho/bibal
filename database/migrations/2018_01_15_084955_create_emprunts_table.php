<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpruntsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emprunts', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');

            $table->integer('Exemplaire')->unsigned();
            $table->foreign('Exemplaire')->references('id')->on('exemplaires');

            $table->integer('Usager')->unsigned();
            $table->foreign('Usager')->references('id')->on('usagers');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emprunts');
    }
}
